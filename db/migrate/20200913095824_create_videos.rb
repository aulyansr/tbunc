class CreateVideos < ActiveRecord::Migration[5.2]
  def change
    create_table :videos do |t|
      t.string :title
      t.string :file
      t.string :video_url
      t.string :thumbnail_image
      t.string :caption
      t.text :description
      t.string :seo_title
      t.string :meta_description
      t.references :post, foreign_key: true

      t.timestamps
    end
  end
end
