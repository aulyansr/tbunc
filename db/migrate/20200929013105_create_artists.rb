class CreateArtists < ActiveRecord::Migration[5.2]
  def change
    create_table :artists do |t|
      t.string :name
      t.string :slug
      t.string :role
      t.text :description
      t.string :seo_title
      t.string :focus_keyphrase
      t.string :meta_description
      t.string :artist_portrait

      t.timestamps
    end
  end
end
