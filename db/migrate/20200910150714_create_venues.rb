class CreateVenues < ActiveRecord::Migration[5.2]
  def change
    create_table :venues do |t|
      t.string :title
      t.string :address
      t.string :postal_code
      t.string :city
      t.string :state_or_province
      t.string :country
      t.text :content
      t.string :phone
      t.string :website

      t.timestamps
    end
  end
end
