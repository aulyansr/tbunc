class CreatePosts < ActiveRecord::Migration[5.2]
  def change
    create_table :posts do |t|
      t.string :title
      t.string :slug
      t.datetime :publish_date
      t.string :status
      t.text :content
      t.string :feature_image
      t.string :post_format
      t.string :seo_title
      t.string :focus_keyphrase
      t.text :meta_description
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
