class CreateImages < ActiveRecord::Migration[5.2]
  def change
    create_table :images do |t|
      t.string :title
      t.string :file
      t.string :caption
      t.string :alternative_text
      t.text :description
      t.string :seo_title
      t.text :meta_description

      t.timestamps
    end
  end
end
