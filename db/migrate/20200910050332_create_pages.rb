class CreatePages < ActiveRecord::Migration[5.2]
  def change
    create_table :pages do |t|
      t.string :title
      t.string :slug
      t.datetime :publish_date
      t.string :status
      t.string :feature_image
      t.text :content
      t.string :seo_title
      t.string :focus_keyphrase
      t.text :meta_description
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
