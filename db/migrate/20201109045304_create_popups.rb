class CreatePopups < ActiveRecord::Migration[5.2]
  def change
    create_table :popups do |t|
      t.text :caption
      t.string :name
      t.string :position
      t.string :image
      t.string :link
      t.string :size

      t.timestamps
    end
  end
end
