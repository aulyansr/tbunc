class CreateCategories < ActiveRecord::Migration[5.2]
  def change
    create_table :categories do |t|
      t.string :name
      t.string :slug
      t.text :description
      t.references :parent
      t.string :seo_title
      t.string :focus_keyphrase
      t.text :meta_description

      t.timestamps
    end
  end
end
