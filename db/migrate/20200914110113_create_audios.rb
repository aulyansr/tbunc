class CreateAudios < ActiveRecord::Migration[5.2]
  def change
    create_table :audios do |t|
      t.string :title
      t.string :caption
      t.string :file
      t.text :description
      t.string :thumbnail_image
      t.references :post, foreign_key: true

      t.timestamps
    end
  end
end
