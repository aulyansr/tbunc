class CreateEvents < ActiveRecord::Migration[5.2]
  def change
    create_table :events do |t|
      t.string :title
      t.string :slug
      t.datetime :publish_date
      t.string :status
      t.string :feature_image
      t.text :content
      t.datetime :start_date
      t.datetime :finish_date
      t.float :price
      t.string :currency
      t.string :seo_title
      t.string :focus_keyphrase
      t.text :meta_description
      t.references :user, foreign_key: true
      t.references :venue, foreign_key: true

      t.timestamps
    end
  end
end
