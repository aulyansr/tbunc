class AddArtistsToVideos < ActiveRecord::Migration[5.2]
  def change
    add_reference :videos, :artist, foreign_key: true
  end
end
