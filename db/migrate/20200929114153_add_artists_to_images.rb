class AddArtistsToImages < ActiveRecord::Migration[5.2]
  def change
    add_reference :images, :artist, foreign_key: true
  end
end
