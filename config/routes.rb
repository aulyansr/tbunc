Rails.application.routes.draw do
  resources :popups
  mount Ckeditor::Engine => '/ckeditor'
  root 'pages#home'
  get 'pages/galleries' => 'pages#galleries'
  get 'setting/general' => 'setups#general'
  resources :setups
  get 'genres/search' => 'genres#search'
  resources :genres
  get 'artists/search' => 'artists#search'
  get 'artists/all' => 'artists#all'
  resources :contacts
  resources :artists
  resources :images
  resources :venues
  get 'events/all' => 'events#all'
  get 'events/preview/:id' => 'events#preview', as: :events_preview
  get 'events/search' => 'events#search'
  get 'events/to_trash/:id' => 'events#to_trash', as: :events_to_trash
  resources :events
  get 'posts/all' => 'posts#all'
  get 'posts/preview/:id' => 'posts#preview', as: :posts_preview
  get 'posts/search' => 'posts#search'
  get 'posts/to_trash/:id' => 'posts#to_trash', as: :posts_to_trash
  resources :posts do
    resources :videos
    resources :audios
  end
  get 'pages/preview/:id' => 'pages#preview', as: :pages_preview
  get 'pages/search' => 'pages#search'
  get 'pages/to_trash/:id' => 'pages#to_trash', as: :pages_to_trash
  resources :pages
  get 'categories/search' => 'categories#search'
  resources :categories
  get 'cms-admin' => 'cms_admin#index'
  devise_for :users
  resources :users
  resources :tags
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
