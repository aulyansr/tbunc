require 'test_helper'

class VideosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @video = videos(:one)
  end

  test "should get index" do
    get videos_url
    assert_response :success
  end

  test "should get new" do
    get new_video_url
    assert_response :success
  end

  test "should create video" do
    assert_difference('Video.count') do
      post videos_url, params: { video: { caption: @video.caption, description: @video.description, file: @video.file, meta_description: @video.meta_description, seo_title: @video.seo_title, title: @video.title, video_url: @video.video_url, videoable_id: @video.videoable_id, videoable_type: @video.videoable_type } }
    end

    assert_redirected_to video_url(Video.last)
  end

  test "should show video" do
    get video_url(@video)
    assert_response :success
  end

  test "should get edit" do
    get edit_video_url(@video)
    assert_response :success
  end

  test "should update video" do
    patch video_url(@video), params: { video: { caption: @video.caption, description: @video.description, file: @video.file, meta_description: @video.meta_description, seo_title: @video.seo_title, title: @video.title, video_url: @video.video_url, videoable_id: @video.videoable_id, videoable_type: @video.videoable_type } }
    assert_redirected_to video_url(@video)
  end

  test "should destroy video" do
    assert_difference('Video.count', -1) do
      delete video_url(@video)
    end

    assert_redirected_to videos_url
  end
end
