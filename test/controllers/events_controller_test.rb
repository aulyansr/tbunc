require 'test_helper'

class EventsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @event = events(:one)
  end

  test "should get index" do
    get events_url
    assert_response :success
  end

  test "should get new" do
    get new_event_url
    assert_response :success
  end

  test "should create event" do
    assert_difference('Event.count') do
      post events_url, params: { event: { content: @event.content, currency: @event.currency, feature_image: @event.feature_image, finish_date: @event.finish_date, focus_keyphrase: @event.focus_keyphrase, meta_description: @event.meta_description, price: @event.price, publish_date: @event.publish_date, seo_title: @event.seo_title, slug: @event.slug, start_date: @event.start_date, status: @event.status, title: @event.title, user_id: @event.user_id, venue_id: @event.venue_id } }
    end

    assert_redirected_to event_url(Event.last)
  end

  test "should show event" do
    get event_url(@event)
    assert_response :success
  end

  test "should get edit" do
    get edit_event_url(@event)
    assert_response :success
  end

  test "should update event" do
    patch event_url(@event), params: { event: { content: @event.content, currency: @event.currency, feature_image: @event.feature_image, finish_date: @event.finish_date, focus_keyphrase: @event.focus_keyphrase, meta_description: @event.meta_description, price: @event.price, publish_date: @event.publish_date, seo_title: @event.seo_title, slug: @event.slug, start_date: @event.start_date, status: @event.status, title: @event.title, user_id: @event.user_id, venue_id: @event.venue_id } }
    assert_redirected_to event_url(@event)
  end

  test "should destroy event" do
    assert_difference('Event.count', -1) do
      delete event_url(@event)
    end

    assert_redirected_to events_url
  end
end
