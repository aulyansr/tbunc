require "application_system_test_case"

class EventsTest < ApplicationSystemTestCase
  setup do
    @event = events(:one)
  end

  test "visiting the index" do
    visit events_url
    assert_selector "h1", text: "Events"
  end

  test "creating a Event" do
    visit events_url
    click_on "New Event"

    fill_in "Content", with: @event.content
    fill_in "Currency", with: @event.currency
    fill_in "Feature image", with: @event.feature_image
    fill_in "Finish date", with: @event.finish_date
    fill_in "Focus keyphrase", with: @event.focus_keyphrase
    fill_in "Meta description", with: @event.meta_description
    fill_in "Price", with: @event.price
    fill_in "Publish date", with: @event.publish_date
    fill_in "Seo title", with: @event.seo_title
    fill_in "Slug", with: @event.slug
    fill_in "Start date", with: @event.start_date
    fill_in "Status", with: @event.status
    fill_in "Title", with: @event.title
    fill_in "User", with: @event.user_id
    fill_in "Venue", with: @event.venue_id
    click_on "Create Event"

    assert_text "Event was successfully created"
    click_on "Back"
  end

  test "updating a Event" do
    visit events_url
    click_on "Edit", match: :first

    fill_in "Content", with: @event.content
    fill_in "Currency", with: @event.currency
    fill_in "Feature image", with: @event.feature_image
    fill_in "Finish date", with: @event.finish_date
    fill_in "Focus keyphrase", with: @event.focus_keyphrase
    fill_in "Meta description", with: @event.meta_description
    fill_in "Price", with: @event.price
    fill_in "Publish date", with: @event.publish_date
    fill_in "Seo title", with: @event.seo_title
    fill_in "Slug", with: @event.slug
    fill_in "Start date", with: @event.start_date
    fill_in "Status", with: @event.status
    fill_in "Title", with: @event.title
    fill_in "User", with: @event.user_id
    fill_in "Venue", with: @event.venue_id
    click_on "Update Event"

    assert_text "Event was successfully updated"
    click_on "Back"
  end

  test "destroying a Event" do
    visit events_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Event was successfully destroyed"
  end
end
