# Preview all emails at http://localhost:3000/rails/mailers/contact_mailer
class ContactMailerPreview < ActionMailer::Preview
  def for_sender_after_submit
    c = Contact.find(2)
    ContactMailer.for_sender_after_submit(c)
  end
end
