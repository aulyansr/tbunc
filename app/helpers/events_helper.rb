module EventsHelper
  def event_meta_data(event)
    setup = Setup.find(1)

    if event.seo_title.present?
      title = event.seo_title
    else
      title = event.title
    end
    
    desc = ActionView::Base.full_sanitizer.sanitize(event.meta_description).truncate(150)
    if event.feature_image.blank? || event.feature_image.nil?
      image = ''
    else
      image = setup.site_url + event.feature_image.url
    end

    keywords = event.focus_keyphrase if event.focus_keyphrase.present?
    
    prepare_meta_tags(
      title: title, description: desc,image:image, keywords: keywords,
      twitter:{
        title: title + ' - ' + setup.site_name,
        description:desc,
        image:image,
        site_name: "#{setup.site_name} | #{title}",
        card: 'summary'
      },
      og:{
        url:request.original_url,
        site_name: setup.site_name,
        title: title + ' - ' + setup.site_name,
        description:desc,
        image:image,
        type:'article'
      }
    )
  end
end
