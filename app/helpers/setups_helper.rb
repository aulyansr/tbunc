module SetupsHelper
    def get_site_url
        site_url = Setup.find(1).site_url
        if site_url.present?
            return site_url
        else
            return '/'
        end
    end

    def get_site_name
        x = Setup.find(1).site_name
        if x.present?
            return x
        else
            return 'Click and Scroll CMS'
        end
    end
    
end
