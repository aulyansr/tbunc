module CategoriesHelper
  def order_by_subcategories
    categories = Array.new
    query = Category.where(parent_id: nil).order(:name)
    query.each do |item|
      categories.push(item)
      if item.subcategories.present?
        item.subcategories.each do |sub|
          categories.push(sub)
        end
      end
    end
    return categories
  end
end
