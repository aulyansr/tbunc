module PopupsHelper

  def show_popup(page_position)

    if page_position == 1
      q = Popup.where(position: page_position)
    else
      q = Popup.where(position: 0)
    end
    render partial: "popups/pop", locals: { data: q }
  end
end
