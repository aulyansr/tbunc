module PagesHelper
  def page_meta_data(page)
    setup = Setup.find(1)
    if page.seo_title.present?
      title = page.seo_title
    else
      title = page.title
    end

    desc = ActionView::Base.full_sanitizer.sanitize(page.meta_description).truncate(150)
    image = request.original_url + ActionController::Base.helpers.asset_path('tbunc-logo.png')
    prepare_meta_tags(
      title: title, description: desc,image:image,
      twitter:{
        title: title + ' - ' + setup.site_name,
        description:desc,
        image:image,
        site_name: "#{setup.site_name} | #{page.title}",
        card: 'summary'
      },
      og:{
        url:request.original_url,
        site_name: setup.site_name,
        title: title + ' - ' + setup.site_name,
        description:desc,
        image:image,
        type:'article'
      }
    )
  end

  def latest_concert_photos
    post_images = Post.where(status: 'publish').where(post_format: 'image')
    latest_concert_photos = Array.new
    post_images.each do |post|
      post.images.each do |image|
        if latest_concert_photos.size <= 8
          latest_concert_photos.push(image.file.url)
        end
      end
    end
    return latest_concert_photos
  end
end
