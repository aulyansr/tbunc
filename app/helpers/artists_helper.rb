module ArtistsHelper
  def artist_meta_data(artist)
    setup = Setup.find(1)

    if artist.seo_title.present?
      title = artist.seo_title
    else
      title = artist.name
    end
    
    desc = ActionView::Base.full_sanitizer.sanitize(artist.meta_description).truncate(150)
    if artist.artist_portrait.blank? || artist.artist_portrait.nil?
      image = ''
    else
      image = setup.site_url + artist.artist_portrait.url
    end

    keywords = artist.focus_keyphrase if artist.focus_keyphrase.present?
    
    prepare_meta_tags(
      title: title, description: desc,image:image, keywords: keywords,
      twitter:{
        title: title + ' - ' + setup.site_name,
        description:desc,
        image:image,
        site_name: "#{setup.site_name} | #{title}",
        card: 'summary'
      },
      og:{
        url:request.original_url,
        site_name: setup.site_name,
        title: title + ' - ' + setup.site_name,
        description:desc,
        image:image,
        type:'article'
      }
    )
  end
end
