module PostsHelper
  def post_meta_data(post)
    setup = Setup.find(1)

    if post.seo_title.present?
      title = post.seo_title
    else
      title = post.title
    end
    
    desc = ActionView::Base.full_sanitizer.sanitize(post.meta_description).truncate(150)
    if post.feature_image.blank? || post.feature_image.nil?
      image = ''
    else
      image = setup.site_url + post.feature_image.url
    end

    keywords = post.focus_keyphrase if post.focus_keyphrase.present?
    
    prepare_meta_tags(
      title: title, description: desc,image:image, keywords: keywords,
      twitter:{
        title: title + ' - ' + setup.site_name,
        description:desc,
        image:image,
        site_name: "#{setup.site_name} | #{title}",
        card: 'summary'
      },
      og:{
        url:request.original_url,
        site_name: setup.site_name,
        title: title + ' - ' + setup.site_name,
        description:desc,
        image:image,
        type:'article'
      }
    )
  end
end
