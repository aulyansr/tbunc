json.extract! image, :id, :title, :image, :caption, :alternative_text, :description, :seo_title, :meta_description, :created_at, :updated_at
json.url image_url(image, format: :json)
