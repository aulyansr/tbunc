json.extract! setup, :id, :site_name, :site_title, :site_description, :site_keyword, :site_url, :site_logo, :created_at, :updated_at
json.url setup_url(setup, format: :json)
