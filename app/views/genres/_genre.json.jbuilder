json.extract! genre, :id, :name, :slug, :description, :created_at, :updated_at
json.url genre_url(genre, format: :json)
