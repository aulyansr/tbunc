json.extract! popup, :id, :caption, :position, :image, :link, :created_at, :updated_at
json.url popup_url(popup, format: :json)
