json.extract! video, :id, :title, :file, :video_url, :caption, :description, :seo_title, :meta_description, :videoable_id, :videoable_type, :created_at, :updated_at
json.url video_url(video, format: :json)
