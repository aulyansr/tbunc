json.extract! category, :id, :name, :slug, :description, :parent_id, :seo_title, :focus_keyphrase, :meta_description, :created_at, :updated_at
json.url category_url(category, format: :json)
