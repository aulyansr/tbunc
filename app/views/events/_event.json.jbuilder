json.extract! event, :id, :title, :slug, :publish_date, :status, :feature_image, :content, :start_date, :finish_date, :price, :currency, :seo_title, :focus_keyphrase, :meta_description, :user_id, :venue_id, :created_at, :updated_at
json.url event_url(event, format: :json)
