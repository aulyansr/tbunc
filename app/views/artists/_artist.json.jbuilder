json.extract! artist, :id, :name, :slug, :role, :description, :seo_title, :focus_keyphrase, :meta_description, :artist_portrait, :created_at, :updated_at
json.url artist_url(artist, format: :json)
