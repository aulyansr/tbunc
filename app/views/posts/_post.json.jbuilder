json.extract! post, :id, :title, :slug, :publish_date, :status, :content, :feature_image, :post_format, :seo_title, :focus_keyphrase, :meta_description, :user_id, :created_at, :updated_at
json.url post_url(post, format: :json)
