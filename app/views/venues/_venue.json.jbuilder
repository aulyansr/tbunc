json.extract! venue, :id, :title, :address, :postal_code, :city, :state_or_province, :country, :content, :phone, :website, :created_at, :updated_at
json.url venue_url(venue, format: :json)
