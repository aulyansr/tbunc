json.extract! page, :id, :title, :slug, :publish_date, :status, :feature_image, :content, :seo_title, :focus_keyphrase, :meta_description, :user_id, :created_at, :updated_at
json.url page_url(page, format: :json)
