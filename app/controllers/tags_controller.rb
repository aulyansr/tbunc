class TagsController < ApplicationController
  before_action :authenticate_user!
  layout 'admin'
  def index
    @tags = Tag.all
  end
end
