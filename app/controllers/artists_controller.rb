class ArtistsController < ApplicationController
  before_action :set_artist, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, except: [:show, :all]
  layout 'admin', except: [:show, :all]
  include PagesHelper
  include ArtistsHelper
  # GET /artists
  # GET /artists.json
  def index
    @artists = Artist.all.order(:name)
    @artists = Kaminari.paginate_array(@artists).page(params[:page]).per(20)
  end

  def search
    if params[:keyword].present?
      @artists = Artist
      if params[:keyword].present?
        if Rails.env.production?
          @artists = @artists.where(["artists.name ILIKE ?", "%#{params[:keyword]}%"])
        else
          @artists = @artists.where(["artists.name LIKE ?", "%#{params[:keyword]}%"])
        end
      end
      @artists = Kaminari.paginate_array(@artists).page(params[:page]).per(20)
    else
      @artists = Artist.order(:name)
      @artists = Kaminari.paginate_array(@artists).page(params[:page]).per(20)
    end
    render 'index'
  end

  def all
    @artists = Artist.all.order(:name)
    @artists = Kaminari.paginate_array(@artists).page(params[:page]).per(20)
    page_meta_data(Page.where(slug: 'all-artists').first) if Page.where(slug: 'all-artists').present?
  end

  # GET /artists/1
  # GET /artists/1.json
  def show
    @genres = @artist.genres
    @artists = Artist.all.order(:name)
    @post_videos = Post.where(status: 'publish').where(post_format: 'video').includes(:videos).where('videos.artist_id = ?', @artist.id).references(:videos)
    @post_images = Post.where(status: 'publish').where(post_format: 'image').includes(:images).where('images.artist_id = ?', @artist.id).references(:images)
    artist_meta_data(@artist)
    @events = Event.where(status: 'publish').limit(5).order(start_date: :asc)
  end

  # GET /artists/new
  def new
    @artist = Artist.new
    @genres = Genre.order(:name)
  end

  # GET /artists/1/edit
  def edit
    @genres = Genre.order(:name)
  end

  # POST /artists
  # POST /artists.json
  def create
    @genres = Genre.order(:name)
    @artist = Artist.new(artist_params)

    respond_to do |format|
      if @artist.save
        format.html { redirect_to artists_path, notice: 'Artist was successfully created.' }
        format.json { render :index, status: :created, location: @artist }
      else
        format.html { render :new }
        format.json { render json: @artist.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /artists/1
  # PATCH/PUT /artists/1.json
  def update
    @genres = Genre.order(:name)
    respond_to do |format|
      if @artist.update(artist_params)
        format.html { redirect_to edit_artist_path(@artist), notice: 'Artist was successfully updated.' }
        format.json { render :edit, status: :ok, location: @artist }
      else
        format.html { render :edit }
        format.json { render json: @artist.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /artists/1
  # DELETE /artists/1.json
  def destroy
    @artist.destroy
    respond_to do |format|
      format.html { redirect_to artists_url, notice: 'Artist was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_artist
      @artist = Artist.friendly.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def artist_params
      params.require(:artist).permit(:name, :slug, :role, :description, :seo_title, :focus_keyphrase, :meta_description, :artist_portrait, :genre_ids => [])
    end
end
