class ApplicationController < ActionController::Base
  before_action :prepare_meta_tags
  protect_from_forgery with: :exception
  include SetupsHelper

  private

  def prepare_meta_tags(options={})
    @setup = Setup.find(1)
    site_name   = @setup.site_name
    if @setup.site_title.present?
      title       = @setup.site_title
    else
      title = ''
    end

    if @setup.site_description.present?
      description       = @setup.site_description
    else
      description = ''
    end

    if @setup.site_keyword.present?
      keywords       = @setup.site_keyword
    else
      keywords = ''
    end

    if @setup.site_logo.present?
      image       = @setup.site_logo.url
    else
      image = ''
    end

    if @setup.site_url.present?
      current_url       = @setup.site_url
    else
      current_url = ''
    end

    # Let's prepare a nice set of defaults
    defaults = {
      site:        site_name,
      title:       title,
      image:       image,
      description: description,
      keywords:    keywords,
      reverse: true,
      twitter: {
        site_name: site_name,
        site: '@Tbunc',
        creator: '@Tbunc',
        card: 'summary',
        description: description,
        title: title,
        image: image
      },
      og: {
        url: current_url,
        site_name: site_name,
        title: title,
        image: image,
        description: description,
        type: 'website'
      }
    }

    options.reverse_merge!(defaults)

    set_meta_tags options
  end
end
