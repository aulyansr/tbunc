class EventsController < ApplicationController
  before_action :set_event, only: [:show, :preview, :edit, :update, :destroy, :to_trash]
  before_action :authenticate_user!, except: [:show, :all]
  layout 'admin', except: [:show, :preview, :all]
  include CategoriesHelper
  include PagesHelper
  include EventsHelper
  # GET /events
  # GET /events.json
  def index
    @events = Event.where.not(status: "trash").order(created_at: :desc)
    @events = Kaminari.paginate_array(@events).page(params[:page]).per(20)
  end

  def search
    if params[:status].present? || params[:category].present? || params[:year].present? || params[:keyword].present?
      @events = Event
      if params[:keyword].present?
        if Rails.env.production?
          @events = @events.where(["events.title ILIKE ?", "%#{params[:keyword]}%"])
        else
          @events = @events.where(["events.title LIKE ?", "%#{params[:keyword]}%"])
        end
      end
      @events = @events.where(status: params[:status]) if params[:status].present?
      @events = @events.joins(:categories).where(categories: {id: params[:category]}).group("events.id") if params[:category].present?

      if params[:year].present?
        @events = @events.where(created_at: Date.new(params[:year].to_i).all_year)
        @events = @events.where(created_at: Date.new(params[:year].to_i, params[:month].to_i).all_month) if params[:month].present?  
      end
      @events = Kaminari.paginate_array(@events).page(params[:page]).per(20)
    else
      @events = Event.order(created_at: :desc)
      @events = Kaminari.paginate_array(@events).page(params[:page]).per(20)
    end
    render 'index'
  end

  def all
    @events = Event.where(status: 'publish').order(start_date: :asc)
    @events = Kaminari.paginate_array(@events).page(params[:page]).per(10)
    page_meta_data(Page.where(slug: 'all-events').first) if Page.where(slug: 'all-events').present?
  end
  
  # GET /events/1
  # GET /events/1.json
  def show
    @events = Event.where(status: 'publish').limit(5).order(start_date: :asc)
    event_meta_data(@event)
  end

  def preview
    render 'show'
  end

  # GET /events/new
  def new
    @event = Event.new
    @users = User.all
    @categories = order_by_subcategories()
    @venues = Venue.all
  end

  # GET /events/1/edit
  def edit
    @users = User.all
    @categories = order_by_subcategories()
    @venues = Venue.all
  end

  # POST /events
  # POST /events.json
  def create
    @users = User.all
    @categories = order_by_subcategories()
    @venues = Venue.all
    @event = Event.new(event_params)
    if params[:publish].present?
      @event.status = 'publish'
    else
      @event.status = 'draft'
    end
    respond_to do |format|
      if @event.save
        format.html { redirect_to edit_event_path(@event), notice: 'Event was successfully created.' }
        format.json { render :edit, status: :created, location: @event }
      else
        format.html { render :new }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /events/1
  # PATCH/PUT /events/1.json
  def update
    @users = User.all
    @categories = order_by_subcategories()
    @venues = Venue.all
    if params[:publish].present?
      @event.status = 'publish'
    else
      @event.status = 'draft'
    end
    respond_to do |format|
      if @event.update(event_params)
        format.html { redirect_to edit_event_path(@event), notice: 'Event was successfully updated.' }
        format.json { render :edit, status: :ok, location: @event }
      else
        format.html { render :edit }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  def to_trash
    respond_to do |format|
      if @event.update(status: 'trash')
        format.html { redirect_to events_path, notice: 'Event was successfully moved to trash.' }
        format.json { render :edit, status: :ok, location: @event }
      else
        format.html { render :edit }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /events/1
  # DELETE /events/1.json
  def destroy
    @event.destroy
    respond_to do |format|
      format.html { redirect_to events_url, notice: 'Event was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_event
      @event = Event.friendly.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def event_params
      params.require(:event).permit(:title, :slug, :publish_date, :status, :feature_image, :content, :start_date, :finish_date, :price, :currency, :seo_title, :focus_keyphrase, :meta_description, :user_id, :venue_id, :tag_list, :category_ids => [])
    end
end
