class CmsAdminController < ApplicationController
  before_action :authenticate_user!
  layout 'admin'
  def index
    @total_post = Post.all.count
    @total_post_publish = Post.where(status:'publish').count
    @total_post_draft = Post.where(status:'draft').count
    @total_event = Event.all.count
    @total_event_publish = Event.where(status: 'publish').count
    @total_event_draft = Event.where(status: 'draft').count
    @total_page = Page.all.count
    @total_artist = Artist.all.count
  end
end
