class GenresController < ApplicationController
  before_action :set_genre, only: [:edit, :update, :destroy]
  before_action :authenticate_user!
  layout 'admin'
  # GET /genres
  # GET /genres.json
  def index
    @genres = Genre.all.order(:name)
    @genres = Kaminari.paginate_array(@genres).page(params[:page]).per(20)
  end

  def search
    if params[:keyword].present?
      @genres = Genre
      if params[:keyword].present?
        if Rails.env.production?
          @genres = @genres.where(["genres.name ILIKE ?", "%#{params[:keyword]}%"])
        else
          @genres = @genres.where(["genres.name LIKE ?", "%#{params[:keyword]}%"])
        end
      end
      @genres = Kaminari.paginate_array(@genres).page(params[:page]).per(20)
    else
      @genres = Genre.order(:name)
      @genres = Kaminari.paginate_array(@genres).page(params[:page]).per(20)
    end
    render 'index'
  end

  # GET /genres/new
  def new
    @genre = Genre.new
  end

  # GET /genres/1/edit
  def edit
  end

  # POST /genres
  # POST /genres.json
  def create
    @genre = Genre.new(genre_params)

    respond_to do |format|
      if @genre.save
        format.html { redirect_to edit_genre_path(@genre), notice: 'Genre was successfully created.' }
        format.json { render :edit, status: :created, location: @genre }
      else
        format.html { render :new }
        format.json { render json: @genre.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /genres/1
  # PATCH/PUT /genres/1.json
  def update
    respond_to do |format|
      if @genre.update(genre_params)
        format.html { redirect_to edit_genre_path(@genre), notice: 'Genre was successfully updated.' }
        format.json { render :edit, status: :ok, location: @genre }
      else
        format.html { render :edit }
        format.json { render json: @genre.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /genres/1
  # DELETE /genres/1.json
  def destroy
    @genre.destroy
    respond_to do |format|
      format.html { redirect_to genres_url, notice: 'Genre was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_genre
      @genre = Genre.friendly.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def genre_params
      params.require(:genre).permit(:name, :slug, :description)
    end
end
