class PagesController < ApplicationController
  before_action :set_page, only: [:show, :preview, :edit, :update, :destroy, :to_trash]
  before_action :authenticate_user!, except: [:show, :home, :galleries]
  layout 'admin', except: [:show, :preview, :home, :galleries]
  include PagesHelper
  # GET /pages
  # GET /pages.json
  def index
    @pages = Page.where.not(status: "trash").order(created_at: :desc)
    @pages = Kaminari.paginate_array(@pages).page(params[:page]).per(20)
  end

  def search
    if params[:status].present? || params[:year].present? || params[:keyword].present?
      @pages = Page
      if params[:keyword].present?
        if Rails.env.production?
          @pages = @pages.where(["pages.title ILIKE ?", "%#{params[:keyword]}%"])
        else
          @pages = @pages.where(["pages.title LIKE ?", "%#{params[:keyword]}%"])
        end
      end
      @pages = @pages.where(status: params[:status]) if params[:status].present?

      if params[:year].present?
        @pages = @pages.where(created_at: Date.new(params[:year].to_i).all_year)
        @pages = @pages.where(created_at: Date.new(params[:year].to_i, params[:month].to_i).all_month) if params[:month].present?
      end
      @pages = Kaminari.paginate_array(@pages).page(params[:page]).per(20)
    else
      @pages = Page.order(created_at: :desc)
      @pages = Kaminari.paginate_array(@pages).page(params[:page]).per(20)
    end
    render 'index'
  end

  def home
    @artists = Artist.all.order(:name)
    @post_videos = Post.where(status: 'publish').where(post_format: 'video')
    if @post_videos.present?
      @first_video = @post_videos.first.videos.last
    end
    @posts =  Post.where(post_format: 'standard').where(status: 'publish').order(publish_date: :desc)
    @events = Event.where(status: 'publish').order(start_date: :asc)

    # latest concert photos query
    @latest_concert_photos = latest_concert_photos()
  end

  def galleries
    @post_images = Post.where(status: 'publish').where(post_format: 'image')
    @post_videos = Post.where(status: 'publish').where(post_format: 'video')
    page_meta_data(Page.where(slug: 'galleries').first) if Page.where(slug: 'galleries').present?
  end

  # GET /pages/1
  # GET /pages/1.json
  def show
    page_meta_data(@page)
  end

  def preview
    render 'show'
  end

  # GET /pages/new
  def new
    @page = Page.new
    @users = User.all
  end

  # GET /pages/1/edit
  def edit
    @users = User.all
  end

  # POST /pages
  # POST /pages.json
  def create
    @page = Page.new(page_params)
    if params[:publish].present?
      @page.status = 'publish'
    else
      @page.status = 'draft'
    end
    respond_to do |format|
      if @page.save
        format.html { redirect_to edit_page_path(@page), notice: 'Page was successfully created.' }
        format.json { render :edit, status: :created, location: @page }
      else
        format.html { render :new }
        format.json { render json: @page.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /pages/1
  # PATCH/PUT /pages/1.json
  def update
    if params[:publish].present?
      @page.status = 'publish'
    else
      @page.status = 'draft'
    end
    respond_to do |format|
      if @page.update(page_params)
        format.html { redirect_to edit_page_path(@page), notice: 'Page was successfully updated.' }
        format.json { render :edit, status: :ok, location: @page }
      else
        format.html { render :edit }
        format.json { render json: @page.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pages/1
  # DELETE /pages/1.json
  def destroy
    @page.destroy
    respond_to do |format|
      format.html { redirect_to pages_url, notice: 'Page was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def to_trash
    respond_to do |format|
      if @page.update(status: 'trash')
        format.html { redirect_to pages_path, notice: 'Page was successfully moved to trash.' }
        format.json { render :edit, status: :ok, location: @page }
      else
        format.html { render :edit }
        format.json { render json: @page.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_page
      @page = Page.friendly.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def page_params
      params.require(:page).permit(:title, :slug, :publish_date, :status, :feature_image, :content, :seo_title, :focus_keyphrase, :meta_description, :user_id)
    end
end
