class SetupsController < ApplicationController
  before_action :set_setup, only: [:general, :update]
  before_action :authenticate_user!
  layout 'admin'

  def general
    
  end

  # PATCH/PUT /setups/1
  # PATCH/PUT /setups/1.json
  def update
    respond_to do |format|
      if @setup.update(setup_params)
        format.html { redirect_to setting_general_path, notice: 'Setup was successfully updated.' }
        format.json { render :general, status: :ok, location: @setup }
      else
        format.html { render :general }
        format.json { render json: @setup.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_setup
      @setup = Setup.find(1)
    end

    # Only allow a list of trusted parameters through.
    def setup_params
      params.require(:setup).permit(:site_name, :site_title, :site_description, :site_keyword, :site_url, :site_logo)
    end
end
