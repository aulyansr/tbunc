class CategoriesController < ApplicationController
  before_action :set_category, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  layout 'admin'
  # GET /categories
  # GET /categories.json
  def index
    @category = nil
    @categories = Category.all.order(:name)
    @categories = Kaminari.paginate_array(@categories).page(params[:page]).per(20)
  end

  def search
    if params[:keyword].present?
      @categories = Category
      if params[:keyword].present?
        if Rails.env.production?
          @categories = @categories.where(["categories.name ILIKE ?", "%#{params[:keyword]}%"])
        else
          @categories = @categories.where(["categories.name LIKE ?", "%#{params[:keyword]}%"])
        end
      end
      @categories = Kaminari.paginate_array(@categories).page(params[:page]).per(20)
    else
      @categories = Category.order(created_at: :desc)
      @categories = Kaminari.paginate_array(@categories).page(params[:page]).per(20)
    end
    render 'index'
  end
  

  # GET /categories/1
  # GET /categories/1.json
  def show
    @categories = @category.subcategories
  end

  # GET /categories/new
  def new
    @category = Category.new
    @category.parent = Category.find(params[:id]) unless params[:id].nil?
    @category_parent = Category.noparent
  end

  # GET /categories/1/edit
  def edit
    @category_parent = Category.noparent
  end

  # POST /categories
  # POST /categories.json
  def create
    @category_parent = Category.noparent
    @category = Category.new(category_params)

    respond_to do |format|
      if @category.save
        format.html { redirect_to categories_path, notice: 'Category was successfully created.' }
        format.json { render :show, status: :created, location: @category }
      else
        format.html { render :new }
        format.json { render json: @category.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /categories/1
  # PATCH/PUT /categories/1.json
  def update
    @category_parent = Category.noparent
    respond_to do |format|
      if @category.update(category_params)
        format.html { redirect_to categories_path, notice: 'Category was successfully updated.' }
        format.json { render :show, status: :ok, location: @category }
      else
        format.html { render :edit }
        format.json { render json: @category.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /categories/1
  # DELETE /categories/1.json
  def destroy
    @category.destroy
    respond_to do |format|
      format.html { redirect_to categories_url, notice: 'Category was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_category
      @category = Category.friendly.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def category_params
      params.require(:category).permit(:name, :slug, :description, :parent_id, :seo_title, :focus_keyphrase, :meta_description)
    end
end
