class PostsController < ApplicationController
  before_action :set_post, only: [:show, :preview, :edit, :update, :destroy, :to_trash]
  before_action :authenticate_user!, except: [:show, :all]
  layout 'admin', except: [:show, :preview, :all]
  include CategoriesHelper
  include PagesHelper
  include PostsHelper
  # GET /posts
  # GET /posts.json
  def index
    @posts = Post.where.not(status: "trash").order(created_at: :desc)
    @posts = Kaminari.paginate_array(@posts).page(params[:page]).per(20)
    @date = Date.today
  end

  def search
    if params[:status].present? || params[:category].present? || params[:year].present? || params[:keyword].present?
      @posts = Post
      if params[:keyword].present?
        if Rails.env.production?
          @posts = @posts.where(["posts.title ILIKE ?", "%#{params[:keyword]}%"])
        else
          @posts = @posts.where(["posts.title LIKE ?", "%#{params[:keyword]}%"])
        end
      end
      @posts = @posts.where(status: params[:status]) if params[:status].present?
      @posts = @posts.joins(:categories).where(categories: {id: params[:category]}).group("posts.id") if params[:category].present?

      if params[:year].present?
        @posts = @posts.where(created_at: Date.new(params[:year].to_i).all_year)
        @posts = @posts.where(created_at: Date.new(params[:year].to_i, params[:month].to_i).all_month) if params[:month].present?  
      end
      @posts = Kaminari.paginate_array(@posts).page(params[:page]).per(20)
    else
      @posts = Post.order(created_at: :desc)
      @posts = Kaminari.paginate_array(@posts).page(params[:page]).per(20)
    end
    render 'index'
  end

  def all
    @posts = Post.where(post_format: 'standard').where(status: 'publish').order(publish_date: :desc)
    @posts = Kaminari.paginate_array(@posts).page(params[:page]).per(9)
    page_meta_data(Page.where(slug: 'all-news').first) if Page.where(slug: 'all-news').present?
  end
  
  # GET /posts/1
  # GET /posts/1.json
  def show
    @events = Event.where(status: 'publish').limit(5).order(start_date: :asc)
    post_meta_data(@post)
  end

  def preview
    render 'show'
  end

  # GET /posts/new
  def new
    @post = Post.new
    @users = User.all
    @categories = order_by_subcategories()
  end

  # GET /posts/1/edit
  def edit
    @users = User.all
    @categories = order_by_subcategories()

    if @post.post_format == 'video'
      @videos = @post.videos
    end
    if @post.post_format == 'audio'
      @audios = @post.audios
    end
  end

  # POST /posts
  # POST /posts.json
  def create
    @categories = order_by_subcategories()
    @post = Post.new(post_params)
    if params[:publish].present?
      @post.status = 'publish'
    else
      @post.status = 'draft'
    end
    if @post.post_format.nil? || @post.post_format.blank?
      @post.post_format = 'standard'
    end
    respond_to do |format|
      if @post.save
        format.html { redirect_to edit_post_path(@post), notice: 'Post was successfully created.' }
        format.json { render :edit, status: :created, location: @post }
      else
        format.html { render :new }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /posts/1
  # PATCH/PUT /posts/1.json
  def update
    @categories = order_by_subcategories()
    if params[:publish].present?
      @post.status = 'publish'
    else
      @post.status = 'draft'
    end
    respond_to do |format|
      if @post.update(post_params)
        format.html { redirect_to edit_post_path(@post), notice: 'Post was successfully updated.' }
        format.json { render :edit, status: :ok, location: @post }
      else
        format.html { render :edit }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  def to_trash
    respond_to do |format|
      if @post.update(status: 'trash')
        format.html { redirect_to posts_path, notice: 'Post was successfully moved to trash.' }
        format.json { render :edit, status: :ok, location: @post }
      else
        format.html { render :edit }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /posts/1
  # DELETE /posts/1.json
  def destroy
    @post.destroy
    respond_to do |format|
      format.html { redirect_to posts_url, notice: 'Post was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post
      @post = Post.friendly.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def post_params
      params.require(:post).permit(:title, :slug, :publish_date, :status, :content, :feature_image, :post_format, :seo_title, :focus_keyphrase, :meta_description, :user_id, :tag_list, :category_ids => [], :images_attributes => [:id, :file, :caption, :description, :artist_id, :_destroy], :videos_attributes => [:id, :file, :caption, :description, :_destroy])
    end

end
