CKEDITOR.editorConfig = function (config) {
  //

  config.toolbar_mini = [
    { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-','JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock','-', 'RemoveFormat' ] }
  ];

  // Define changes to default configuration here. For example:
  config.language = 'en';
  // config.uiColor = '#AADC6E';

  config.allowedContent = true;
  config.filebrowserUploadMethod = 'form';

  config.toolbar = "mini";

  config.height = 200;

  // ... rest of the original config.js  ...
}
