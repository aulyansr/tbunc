// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require jquery3
//= require popper
//= require bootstrap
//= require activestorage
// require turbolinks
// require_tree .
$(document).ready(function(){
    $(".owl-carousel").owlCarousel({
        autoPlay: 3000, //Set AutoPlay to 3 seconds
        items : 3,
        loop: true,
        center: true
    });
    $('.modal').on('hidden.bs.modal', function () {
        var $this = $(this).find('iframe'),
        tempSrc = $this.attr('src');
        $this.attr('src', "");
        $this.attr('src', tempSrc);
        $(this).find('video')[0].pause();
    });
});