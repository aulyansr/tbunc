// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require rails-ujs
//= require admin/bootstrap/bootstrap.bundle
// require bootstrap
//= require popper
//= require admin/app-style-switcher
//= require admin/app.init
//= require admin/app
//= require admin/perfect-scrollbar.jquery.min
//= require admin/sidebarmenu
//= require admin/waves
//= require admin/sparkline
//= require admin/dropify
//= require admin/custom
//= require admin/jquery.steps.min
//= require admin/moment
//= require admin/bootstrap-material-datetimepicker-custom
//= require toastr
//= require selectize
//= require ckeditor/init
//= require cocoon
// require activestorage
// require turbolinks
// require_tree .
// global toastr
toastr.options = {
  "closeButton": true,
  "progressBar": false,
  "debug": false,
  "positionClass": "toast-top-center",
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}
