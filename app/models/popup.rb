class Popup < ApplicationRecord
  mount_uploader :image, FeatureUploader
  POSITION = ["All Page", "Homepage"]
  SIZE = {small: "modal-sm", default: "modal-default", large: "modal-lg", xlarge: "modal-xl"}
  validates :name, presence: true
  validates :size, presence: true
  validates :position, presence: true
end
