class Video < ApplicationRecord
  belongs_to :post
  # has_many :artists , dependent: :destroy
  mount_uploader :file, VideoUploader
  mount_uploader :thumbnail_image, FeatureUploader
  # process_in_background :file
  validates :title, presence: true
  validates :file, file_size: { in: 1.kilobyte..200.megabyte }
  validates :thumbnail_image, file_size: { in: 1.kilobyte..1.megabyte }

  def set_success(format, opts)
    self.success = true
  end

  # def thumbnail_url
  #   if video_processing
  #     ""
  #   else
  #     video.thumb.url
  #   end
  # end

  # All attachables should have the image_path method, this is sent to API
  # def image_path
  #   thumbnail_url
  # end

  # def video_path
  #   # Serve the raw video until the processing is done.
  #   if video_processing
  #     video.url
  #   else
  #     video.rescaled.url
  #   end
  # end
end
