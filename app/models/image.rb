class Image < ApplicationRecord
  has_and_belongs_to_many :posts
  has_many :artist , dependent: :destroy
  mount_uploader :file, ImageUploader
end
