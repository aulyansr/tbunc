class Contact < ApplicationRecord
  PILIHAN = ["Event Organizer", "Talent DJ's or Band", "Sponsorship and Collaboration", "let's just have a cup of coffee!"]
  validates :name, presence: true
  validates :email, presence: true
end
