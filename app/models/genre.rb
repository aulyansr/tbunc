class Genre < ApplicationRecord
    has_and_belongs_to_many :artists
    extend FriendlyId
    friendly_id :name, use: :slugged

    validates :name, presence: true
end
