class User < ApplicationRecord
  include ActiveModel::Validations
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable, :registerable
  devise :database_authenticatable, :recoverable, :rememberable, :validatable
  ROLES = {administrator:'administrator', editor: 'editor', author: 'author'}
  mount_uploader :avatar, AvatarUploader
  validates :role, presence: true
  validates :username, presence: true
  validates :avatar, file_size: { in: 1.kilobyte..200.kilobyte }
end
