class Post < ApplicationRecord
  has_and_belongs_to_many :categories
  has_and_belongs_to_many :images
  has_many :videos , dependent: :destroy
  has_many :audios , dependent: :destroy
  belongs_to :user
  acts_as_taggable_on :tags
  mount_uploader :feature_image, FeatureUploader
  accepts_nested_attributes_for :images, allow_destroy: true
  extend FriendlyId
  friendly_id :title, use: :slugged
  STATUS = [:publish, :draft, :trash]
  POST_FORMAT = [:standard, :image, :video, :audio]
  validates :title, presence: true
  validates :feature_image, file_size: { in: 1.kilobyte..1.megabyte }
  validates :publish_date, presence: true

  scope :drafted, -> {where(status: 'draft')}
  scope :published, -> {where(status: 'publish')}
  scope :trashed, -> {where(status: 'trash')}
end
