class Category < ApplicationRecord
  has_and_belongs_to_many :posts
  has_and_belongs_to_many :events
  has_many :subcategories, :class_name => "Category", :foreign_key => :parent_id, :dependent => :destroy
  belongs_to :parent, :class_name => "Category", :foreign_key => :parent_id, optional: true

  extend FriendlyId
  friendly_id :name, use: :slugged
  scope :noparent, -> {where(parent_id: [nil,""])}
  validates :name, presence: true

end
