class Setup < ApplicationRecord
    mount_uploader :site_logo, AvatarUploader
    validates :site_name, presence: true
end
