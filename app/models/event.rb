class Event < ApplicationRecord
  has_and_belongs_to_many :categories
  belongs_to :user
  belongs_to :venue, optional: true
  acts_as_taggable_on :tags
  mount_uploader :feature_image, FeatureUploader
  extend FriendlyId
  friendly_id :title, use: :slugged
  STATUS = [:publish, :draft, :trash]
  CURRENCY = [:usd, :idr, :eur, :sgd, :aud, :jpy]
  validates :title, presence: true
  validates :feature_image, file_size: { in: 1.kilobyte..1.megabyte }
  validates :price, numericality: {greater_than_or_equal_to: 0}
  validates :publish_date, presence: true
  scope :drafted, -> {where(status: 'draft')}
  scope :published, -> {where(status: 'publish')}
  scope :trashed, -> {where(status: 'trash')}
end
