class Page < ApplicationRecord
  belongs_to :user
  extend FriendlyId
  friendly_id :title, use: :slugged
  STATUS = [:publish, :draft, :trash]
  scope :drafted, -> {where(status: 'draft')}
  scope :published, -> {where(status: 'publish')}
  scope :trashed, -> {where(status: 'trash')}
  validates :title, presence: true
end
