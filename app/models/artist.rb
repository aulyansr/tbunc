class Artist < ApplicationRecord
    has_and_belongs_to_many :genres
    # belongs_to :image
    # belongs_to :video
    # belongs_to :audio
    extend FriendlyId
    friendly_id :name, use: :slugged
    mount_uploader :artist_portrait, FeatureUploader
    validates :name, presence: true
end
