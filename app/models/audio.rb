class Audio < ApplicationRecord
  belongs_to :post
  mount_uploader :file, AudioUploader
  mount_uploader :thumbnail_image, FeatureUploader

  validates :title, presence: true
  validates :file, presence: true, file_size: { in: 1.kilobyte..50.megabyte }
  validates :thumbnail_image, file_size: { in: 1.kilobyte..1.megabyte }
end
