class ContactMailer < ApplicationMailer
  default from: 'system@tbunc.com'
  layout 'mailer'

  def for_sender_after_submit(data)
    mail to: data.email, subject: 'Thanks for Contacting Us'
  end

  def for_admin_after_submit(data)
    @data = data
    mail to: 'info@tbunc.com', subject: 'Ada Kontak Masuk'
  end
end
